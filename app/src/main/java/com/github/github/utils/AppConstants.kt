package com.github.github.utils

/**
 * Created by hp on 27-05-2018.
 */
object AppConstants {
    val URL = "URL"
    val TRANSACTION = "TRANSACTION"
    val BACKSTACK = "BACKSTACK"
    val SCREENNAME = "SCREENNAME"
    val ADD_FRAGMENT = "add"
    val REPLACE_FRAGMENT = "replace"

    object Models{
        val RepositoryDetailModel = "RepositoryDetailModel"
    }

    object ScreenName{
        val RepositoryDetailFragment = "REPOSITORY_DETAIL_FRAGMENT"
    }
}