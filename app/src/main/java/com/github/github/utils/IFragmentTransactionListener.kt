package com.github.github.utils

import android.os.Bundle

/**
 * Created by hp on 26-05-2018.
 *
 * For transaction between fragments through Activity
 */
interface IFragmentTransactionListener {
    fun onFragmentTransaction(bundle: Bundle)
}