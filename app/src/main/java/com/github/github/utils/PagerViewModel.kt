package com.github.github.utils

import android.support.v4.app.Fragment
import com.github.github.base.BaseFragment

/**
 * Created by hp on 26-05-2018.
 */
class PagerViewModel<T:Fragment>( title:String,fragment:T){
    var title : String
    var fragment : Fragment
    init {
        this.title = title
        this.fragment = fragment
    }
}