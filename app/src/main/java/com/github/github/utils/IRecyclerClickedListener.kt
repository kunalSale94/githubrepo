package com.github.github.utils

/**
 * Created by hp on 25-05-2018.
 */
interface IRecyclerClickedListener {
    fun onRecyclerItemClicked(position:Int)
}