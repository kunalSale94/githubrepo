package com.github.github.utils

import android.databinding.BindingAdapter
import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.github.github.base.BaseRecyclerAdapter
import com.github.github.base.BaseViewModel

/**
 * Created by hp on 26-05-2018.
 */

// Notifies the adapter on item list changes
@BindingAdapter("android:adapter")
fun <T : BaseViewModel,VDB : ViewDataBinding,VH:RecyclerView.ViewHolder> setAdapter( recyclerView:RecyclerView?, itemList: ArrayList<T>){
    if(recyclerView?.adapter is BaseRecyclerAdapter<*,*,*>){
        val adapter : BaseRecyclerAdapter<T,VDB,VH>? = recyclerView?.adapter as BaseRecyclerAdapter<T,VDB,VH>
        adapter?.updateList(itemList)
    }
}

// to set the image in ImageView
@BindingAdapter("android:src")
fun  setImage( imageView:ImageView?, url: String){
    ImageLoader.loadUserImage(imageView!!,url)
}

// toggles the visibility
@BindingAdapter("android:visibility")
fun  setVisibility(view: View?, visibility: Boolean){
    if(visibility){
        view?.visibility = View.VISIBLE
    }
    else{
        view?.visibility = View.GONE
    }
}