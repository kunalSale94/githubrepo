package com.github.github.utils

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.github.github.R

/**
 * Wrapper class for Image Loading.
 *
 * Internally uses Glide, but can be hot-swapped for anything else.
 */
object ImageLoader {
    private val userImageRequestOptions = RequestOptions()
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .placeholder(R.drawable.ic_avatar)

    /**
     * Load a user's display image based on his Email.
     */
    fun loadUserImage(imageView: ImageView, url: String) {
        Glide.with(imageView)
                .applyDefaultRequestOptions(userImageRequestOptions)
                .load(url)
                .into(imageView)
    }

}