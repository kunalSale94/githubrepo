package com.github.github.details.branches.models

import android.databinding.Bindable
import com.github.github.BR
import com.github.github.base.BaseViewModel
import com.github.github.details.branches.responses.RepositoryBranchesResponseModel

/**
 * Created by hp on 27-05-2018.
 */
class ItemRepositoryBranchModel : BaseViewModel() {
    var name: String? = null
        @Bindable get() = field
        set(value) {
            field = value
            notifyPropertyChanged(BR.name)
        }
    var commitsModel: CommitsModel? = null
        @Bindable get() = field
        set(commitsModel) {
            field = commitsModel
            notifyPropertyChanged(BR.commitsModel)
        }

    fun setBranchesViewModel(repositoryBranchesResponseModel: RepositoryBranchesResponseModel?){
        name = repositoryBranchesResponseModel?.name
        commitsModel = CommitsModel()
        commitsModel?.setCommitViewModel(repositoryBranchesResponseModel?.commit)
    }
}