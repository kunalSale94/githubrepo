package com.github.github.details.branches

import com.github.github.data.GitHubRetrofitAdapter
import com.github.github.data.GitHubService
import com.github.github.details.branches.models.BranchesViewModel
import com.github.github.details.branches.models.ItemRepositoryBranchModel
import com.github.github.details.branches.responses.RepositoryBranchesResponseModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by hp on 27-05-2018.
 */
class RepositoryBranchesPresenter(branchesViewModel: BranchesViewModel) {

    var branchesViewModel: BranchesViewModel

    init {
        this.branchesViewModel = branchesViewModel
    }

    fun callBranchesApi(branchesUrl:String?){
        branchesViewModel.isLoading = true
        val gitHubService: GitHubService = GitHubRetrofitAdapter.getRetrofitInstance().create(GitHubService::class.java)
        val call: Call<List<RepositoryBranchesResponseModel>> = gitHubService.repositoriesBranches(branchesUrl!!)
        call.enqueue(object : Callback<List<RepositoryBranchesResponseModel>> {
            override fun onFailure(call: Call<List<RepositoryBranchesResponseModel>>?, t: Throwable?) {
                branchesViewModel.isLoading = false
            }

            override fun onResponse(call: Call<List<RepositoryBranchesResponseModel>>?, response: Response<List<RepositoryBranchesResponseModel>>?) {
                branchesViewModel.isLoading = false
                val branchesList: List<RepositoryBranchesResponseModel>? = response?.body()
                val branchModelItems: ArrayList<ItemRepositoryBranchModel> = ArrayList()
                for ( branchResponseModel in branchesList!!){
                    val branchViewModelItem: ItemRepositoryBranchModel = ItemRepositoryBranchModel()
                    branchViewModelItem.setBranchesViewModel(branchResponseModel)
                    branchModelItems.add(branchViewModelItem)
                }
                branchesViewModel.updateBranchesList(branchModelItems)
            }
        })
    }

}