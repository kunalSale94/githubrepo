package com.github.github.details.branches.responses

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Commit {

    @SerializedName("sha")
    @Expose
    var sha: String? = null
    @SerializedName("url")
    @Expose
    var url: String? = null

}
