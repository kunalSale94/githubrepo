package com.github.github.details.languages

import android.databinding.Bindable
import com.github.github.BR
import com.github.github.base.BaseViewModel


/**
 * Created by hp on 27-05-2018.
 */
class ItemRepositoryLangauageModel : BaseViewModel() {
    var language: String? = null
        @Bindable get() = field
        set(value) {
            field = value
            notifyPropertyChanged(BR.language)
        }

    var languageID: Double? = null
        @Bindable get() = field
        set(value) {
            field = value
            notifyPropertyChanged(BR.languageID)
        }
}