package com.github.github.details.branches.models

import android.databinding.ObservableArrayList
import com.github.github.base.BaseViewModel
import com.github.github.details.branches.models.ItemRepositoryBranchModel

/**
 * Created by hp on 27-05-2018.
 *
 * Holds the List of Observable for Branches
 */
class BranchesViewModel : BaseViewModel() {

    var branchModelsListItem: ObservableArrayList<ItemRepositoryBranchModel> = ObservableArrayList()
        get() = field

    fun updateBranchesList(branchModelsListItem:ArrayList<ItemRepositoryBranchModel>){
        this.branchModelsListItem.clear()
        this.branchModelsListItem.addAll(branchModelsListItem)
    }
}