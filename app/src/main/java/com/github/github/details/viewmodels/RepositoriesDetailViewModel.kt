package com.github.github.details.viewmodels

import android.arch.lifecycle.ViewModel
import com.github.github.details.RepositoryDetailModel

/**
 * Created by hp on 28-05-2018.
 *
 *  ViewModel to persist data for repository detail model
 */
class RepositoriesDetailViewModel : ViewModel() {

    private var repositoryDetailModel: RepositoryDetailModel ?= null

    fun setRepositoryDetailModel(repositoryDetailModel: RepositoryDetailModel) {
        this.repositoryDetailModel = repositoryDetailModel
    }

    fun getRepositoryDetailModel() = repositoryDetailModel
}