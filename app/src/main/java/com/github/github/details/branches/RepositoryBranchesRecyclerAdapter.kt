package com.github.github.details.branches

import android.support.v7.widget.RecyclerView
import com.github.github.R
import com.github.github.base.BaseRecyclerAdapter
import com.github.github.databinding.ItemBranchesRowLayoutBinding
import com.github.github.details.branches.models.ItemRepositoryBranchModel

/**
 * Created by hp on 27-05-2018.
 */
class RepositoryBranchesRecyclerAdapter(itemsList:ArrayList<ItemRepositoryBranchModel>) : BaseRecyclerAdapter<ItemRepositoryBranchModel,ItemBranchesRowLayoutBinding,RepositoryBranchesRecyclerAdapter.RepositoryBranchesViewHolder>(itemsList) {

    override fun getItemLayout(): Int {
        return R.layout.item_branches_row_layout
    }

    override fun getViewHolder(viewDataBinding: ItemBranchesRowLayoutBinding): RepositoryBranchesViewHolder {
        return RepositoryBranchesViewHolder(viewDataBinding)
    }

    override fun onBindViewHolder(holder: RepositoryBranchesViewHolder, position: Int) {
        super.onBindViewHolder(holder, position)
        holder.viewDataBinding.viewModel = itemsList.get(holder.adapterPosition)
    }

    class RepositoryBranchesViewHolder(viewDataBinding: ItemBranchesRowLayoutBinding) : RecyclerView.ViewHolder(viewDataBinding.root){
        var viewDataBinding: ItemBranchesRowLayoutBinding
        init {
            this.viewDataBinding = viewDataBinding
        }
    }
}