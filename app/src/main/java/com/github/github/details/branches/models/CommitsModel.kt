package com.github.github.details.branches.models

import android.databinding.Bindable
import com.github.github.BR
import com.github.github.base.BaseViewModel
import com.github.github.details.branches.responses.Commit

/**
 * Created by hp on 27-05-2018.
 */
class CommitsModel : BaseViewModel() {
    var sha: String? = null
        @Bindable get() = field
        set(value) {
            field = value
            notifyPropertyChanged(BR.sha)
        }
    var url: String? = null
        @Bindable get() = field
        set(value) {
            field = value
            notifyPropertyChanged(BR.url)
        }

    fun setCommitViewModel(commit:Commit?){
        sha = commit?.sha
        url = commit?.url
    }
}