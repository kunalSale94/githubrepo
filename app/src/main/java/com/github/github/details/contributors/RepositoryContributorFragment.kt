package com.github.github.details.contributors

import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.github.github.R
import com.github.github.base.BaseFragment
import com.github.github.databinding.FragmentContributorsLayoutBinding
import com.github.github.details.contributors.models.ItemRepositoryContributorModel
import com.github.github.details.contributors.models.RepositoryContributorModel
import com.github.github.utils.AppConstants

/**
 * Created by hp on 26-05-2018.
 */
class RepositoryContributorFragment() : BaseFragment<FragmentContributorsLayoutBinding>() {

    var contributorUrl: String? = null
    lateinit var contributorPresenter: RepositoryContributorPresenter

    companion object {
        fun getInstance(bundle: Bundle): RepositoryContributorFragment {
            val repositoryContributorFragment: RepositoryContributorFragment = RepositoryContributorFragment()
            repositoryContributorFragment.arguments = bundle
            return repositoryContributorFragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        contributorPresenter = RepositoryContributorPresenter(RepositoryContributorModel())
        val bundle: Bundle? = arguments
        contributorUrl = bundle?.getString(AppConstants.URL)
    }

    override fun getFragmentLayout(): Int {
        return R.layout.fragment_contributors_layout
    }

    override fun onBindViewInflated() {
        super.onBindViewInflated()
        viewDataBinding.presenter = contributorPresenter
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        callContributorUrl()
    }

    private fun init() {
        val contributorsAdapter = ContributorsRecyclerAdapter(ArrayList<ItemRepositoryContributorModel>())
        val rvContributors: RecyclerView = viewDataBinding.recyclerViewContributors
        val linearLayoutManager: LinearLayoutManager = LinearLayoutManager(context)
        rvContributors.addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.VERTICAL))
        rvContributors.layoutManager = linearLayoutManager
        rvContributors.adapter = contributorsAdapter
    }

    private fun callContributorUrl() {
        contributorPresenter.callContributorsApi(contributorUrl)
    }
}