package com.github.github.details.languages

import android.databinding.ObservableArrayList
import com.github.github.base.BaseViewModel

/**
 * Created by hp on 27-05-2018.
 *
 * Holds the List of Observable for Languages
 */
class RepositoryLanguageModel : BaseViewModel() {
    var languageListModelsList: ObservableArrayList<ItemRepositoryLangauageModel> = ObservableArrayList()
        get() = field

    fun updateLanguagesList(repositoriesModelItems:ArrayList<ItemRepositoryLangauageModel>){
        languageListModelsList.clear()
        languageListModelsList.addAll(repositoriesModelItems)
    }
}