package com.github.github.details.branches.responses

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class RepositoryBranchesResponseModel {

    @SerializedName("name")
    @Expose
    var name: String? = null
    @SerializedName("commit")
    @Expose
    var commit: Commit? = null

}
