package com.github.github.details

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.View
import com.github.github.R
import com.github.github.base.BaseFragment
import com.github.github.base.DetailsPagerAdapter
import com.github.github.databinding.FragmentRepositoriesDetailsLayoutBinding
import com.github.github.details.branches.RepositoryBranchesFragment
import com.github.github.details.contributors.RepositoryContributorFragment
import com.github.github.details.languages.RepositoryLanguagesFragment
import com.github.github.details.viewmodels.RepositoriesDetailViewModel
import com.github.github.repositories.models.ItemRepositoriesModel
import com.github.github.utils.AppConstants
import com.github.github.utils.PagerViewModel

/**
 * Created by hp on 26-05-2018.
 */
class RepositoriesDetailsFragment : BaseFragment<FragmentRepositoriesDetailsLayoutBinding> (){

    var repositoryDetailViewModel : RepositoriesDetailViewModel ?= null
    var repositoryDetailModel : RepositoryDetailModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
            repositoryDetailViewModel = ViewModelProviders.of(this).get(RepositoriesDetailViewModel::class.java)
        val bundle : Bundle? = arguments
        if(bundle != null){
            repositoryDetailModel = bundle.getParcelable<RepositoryDetailModel>(AppConstants.Models.RepositoryDetailModel)
            repositoryDetailViewModel?.setRepositoryDetailModel(repositoryDetailModel!!)
        }
        else{
            if(repositoryDetailViewModel != null && repositoryDetailViewModel?.getRepositoryDetailModel()!=null){
                repositoryDetailModel = repositoryDetailViewModel?.getRepositoryDetailModel()
            }
        }

    }
    override fun getFragmentLayout(): Int {
        return R.layout.fragment_repositories_details_layout
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {
        val detailPagerAdapter : DetailsPagerAdapter<android.support.v4.app.Fragment> = DetailsPagerAdapter(getDetailsPagerList(),activity?.supportFragmentManager!!);
        viewDataBinding.tabLayoutDetails.setupWithViewPager(viewDataBinding.viewPagerDetails)
        viewDataBinding.viewPagerDetails.adapter = detailPagerAdapter
    }

    fun getDetailsPagerList():ArrayList<PagerViewModel<android.support.v4.app.Fragment>>{
        val detailsPagerList : ArrayList<PagerViewModel<android.support.v4.app.Fragment>> = ArrayList()
        detailsPagerList.add(PagerViewModel<android.support.v4.app.Fragment>( getString(R.string.contributor), RepositoryContributorFragment.getInstance(getBundle(repositoryDetailModel?.contributorsUrl))))
        detailsPagerList.add(PagerViewModel<android.support.v4.app.Fragment>( getString(R.string.languages), RepositoryLanguagesFragment.getInstance(getBundle(repositoryDetailModel?.languagesUrl))))
        detailsPagerList.add(PagerViewModel<android.support.v4.app.Fragment>( getString(R.string.branches), RepositoryBranchesFragment.getInstance(getBundle(repositoryDetailModel?.branchesUrl))))
        return detailsPagerList
    }

    // Get the Bundle for the ViewPager item
    private fun  getBundle(url: String?): Bundle {
        val bundle : Bundle = Bundle()
        bundle.putString(AppConstants.URL,url)
        return bundle
    }
}