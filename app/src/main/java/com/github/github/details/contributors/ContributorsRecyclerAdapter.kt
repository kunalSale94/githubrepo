package com.github.github.details.contributors

import android.support.v7.widget.RecyclerView
import com.github.github.R
import com.github.github.base.BaseRecyclerAdapter
import com.github.github.databinding.ItemContributorRowLayoutBinding
import com.github.github.details.contributors.models.ItemRepositoryContributorModel

/**
 * Created by hp on 27-05-2018.
 */
class ContributorsRecyclerAdapter(itemsList:ArrayList<ItemRepositoryContributorModel>) : BaseRecyclerAdapter<ItemRepositoryContributorModel,ItemContributorRowLayoutBinding,ContributorsRecyclerAdapter.ContributorsViewHolder>(itemsList) {

    override fun getItemLayout(): Int {
        return R.layout.item_contributor_row_layout
    }

    override fun getViewHolder(viewDataBinding: ItemContributorRowLayoutBinding): ContributorsViewHolder {
        return ContributorsViewHolder(viewDataBinding)
    }

    override fun onBindViewHolder(holder: ContributorsViewHolder, position: Int) {
        super.onBindViewHolder(holder, position)
        holder.viewDataBinding.viewModel = itemsList.get(holder.adapterPosition)
    }

    class ContributorsViewHolder(viewDataBinding: ItemContributorRowLayoutBinding) : RecyclerView.ViewHolder(viewDataBinding.root){
        var viewDataBinding: ItemContributorRowLayoutBinding
        init {
            this.viewDataBinding = viewDataBinding
        }
    }
}