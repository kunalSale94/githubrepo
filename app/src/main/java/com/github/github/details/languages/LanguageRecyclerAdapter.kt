package com.github.github.details.languages

import android.support.v7.widget.RecyclerView
import com.github.github.R
import com.github.github.base.BaseRecyclerAdapter
import com.github.github.databinding.ItemLanguageRowLayoutBinding

/**
 * Created by hp on 27-05-2018.
 */
class LanguageRecyclerAdapter(itemsList:ArrayList<ItemRepositoryLangauageModel>): BaseRecyclerAdapter<ItemRepositoryLangauageModel,ItemLanguageRowLayoutBinding, LanguageRecyclerAdapter.LanguageViewHolder>(itemsList) {

    override fun getItemLayout(): Int {
        return R.layout.item_language_row_layout
    }

    override fun getViewHolder(viewDataBinding: ItemLanguageRowLayoutBinding): LanguageViewHolder {
        return LanguageViewHolder(viewDataBinding)
    }

    override fun onBindViewHolder(holder: LanguageViewHolder, position: Int) {
        super.onBindViewHolder(holder, position)
        holder.viewDataBinding.viewModel = itemsList.get(holder.adapterPosition)
    }

    class LanguageViewHolder(viewDataBinding: ItemLanguageRowLayoutBinding) : RecyclerView.ViewHolder(viewDataBinding.root){
        var viewDataBinding: ItemLanguageRowLayoutBinding
        init {
            this.viewDataBinding = viewDataBinding
        }
    }
}