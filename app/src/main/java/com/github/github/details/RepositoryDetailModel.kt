package com.github.github.details

import android.databinding.Bindable
import android.os.Parcel
import android.os.Parcelable

/**
 * Created by hp on 28-05-2018.
 */
class RepositoryDetailModel() : Parcelable {
    var branchesUrl: String? = null
        get() = field
        set(branchesUrl) {
            field = branchesUrl
        }
    var contributorsUrl: String? = null
        get() = field
        set(contributorsUrl) {
            field = contributorsUrl
        }
    var languagesUrl: String? = null
        get() = field
        set(languagesUrl) {
            field = languagesUrl
        }

    constructor(parcel: Parcel) : this() {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {

    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<RepositoryDetailModel> {
        override fun createFromParcel(parcel: Parcel): RepositoryDetailModel {
            return RepositoryDetailModel(parcel)
        }

        override fun newArray(size: Int): Array<RepositoryDetailModel?> {
            return arrayOfNulls(size)
        }
    }
}