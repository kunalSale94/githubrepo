package com.github.github.details.branches

import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.github.github.R
import com.github.github.base.BaseFragment
import com.github.github.databinding.FragmentBranchesLayoutBinding
import com.github.github.details.branches.models.BranchesViewModel
import com.github.github.details.branches.models.ItemRepositoryBranchModel
import com.github.github.utils.AppConstants

/**
 * Created by hp on 26-05-2018.
 */
class RepositoryBranchesFragment() : BaseFragment<FragmentBranchesLayoutBinding>() {

    var branchesUrl :String? = null
    var repositoryBranchPresenter : RepositoryBranchesPresenter? = null

    companion object {
        fun getInstance(bundle: Bundle): RepositoryBranchesFragment {
            val repositoryBranchesFragment : RepositoryBranchesFragment = RepositoryBranchesFragment()
            repositoryBranchesFragment.arguments = bundle
            return repositoryBranchesFragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val bundle : Bundle? = arguments
        branchesUrl = bundle?.getString(AppConstants.URL)
    }

    override fun getFragmentLayout(): Int {
        return R.layout.fragment_branches_layout
    }

    override fun onBindViewInflated() {
        super.onBindViewInflated()
        repositoryBranchPresenter = RepositoryBranchesPresenter(BranchesViewModel())
        viewDataBinding.presenter = repositoryBranchPresenter
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        callRepositoryBranchesApi()
    }

    private fun init() {
        val branchesRecyclerAdapter = RepositoryBranchesRecyclerAdapter(ArrayList<ItemRepositoryBranchModel>())
        val rvBranches: RecyclerView = viewDataBinding.recyclerViewBranches
        val linearLayoutManager: LinearLayoutManager = LinearLayoutManager(context);
        rvBranches.addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.VERTICAL))
        rvBranches.layoutManager = linearLayoutManager
        rvBranches.adapter = branchesRecyclerAdapter
    }

    private fun callRepositoryBranchesApi() {
        repositoryBranchPresenter?.callBranchesApi(branchesUrl)
    }
}