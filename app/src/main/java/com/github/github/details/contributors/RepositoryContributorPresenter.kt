package com.github.github.details.contributors

import com.github.github.data.GitHubRetrofitAdapter
import com.github.github.data.GitHubService
import com.github.github.details.contributors.models.ItemRepositoryContributorModel
import com.github.github.details.contributors.models.RepositoryContributorModel
import com.github.github.details.contributors.responses.RepositoryContributorsResponseModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by hp on 27-05-2018.
 */
class RepositoryContributorPresenter(contributorsModel: RepositoryContributorModel) {

    var contributorsModel: RepositoryContributorModel

    init {
        this.contributorsModel = contributorsModel
    }

    fun callContributorsApi(contributorsString:String?){
        contributorsModel.isLoading = true
        val gitHubService: GitHubService = GitHubRetrofitAdapter.getRetrofitInstance().create(GitHubService::class.java)
        val call: Call<List<RepositoryContributorsResponseModel>> = gitHubService.repositoriesContributors(contributorsString!!)
        call.enqueue(object : Callback<List<RepositoryContributorsResponseModel>> {
            override fun onFailure(call: Call<List<RepositoryContributorsResponseModel>>?, t: Throwable?) {
                contributorsModel.isLoading = false
            }

            override fun onResponse(call: Call<List<RepositoryContributorsResponseModel>>?, response: Response<List<RepositoryContributorsResponseModel>>?) {
                val contributorsList: List<RepositoryContributorsResponseModel>? = response?.body()
                val contributorModelListItem: ArrayList<ItemRepositoryContributorModel> = ArrayList()
                for ( contributorResponseModel in contributorsList!!){
                    val contributorModelItem: ItemRepositoryContributorModel = ItemRepositoryContributorModel()
                    contributorModelItem.setContributorModel(contributorResponseModel)
                    contributorModelListItem.add(contributorModelItem)
                }
                contributorsModel.isLoading = false
                contributorsModel.updateContributorsList(contributorModelListItem)
            }
        })
    }
}