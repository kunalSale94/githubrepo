package com.github.github.details.languages

import com.github.github.data.GitHubRetrofitAdapter
import com.github.github.data.GitHubService
import com.google.gson.internal.LinkedTreeMap
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by hp on 27-05-2018.
 */
class RepositoryLanguagesPresenter (langauageModel: RepositoryLanguageModel){

    var languagesModel: RepositoryLanguageModel

    init {
        this.languagesModel = langauageModel
    }

    fun callRepositoryLanguageApi(languageUrl : String?){
        languagesModel.isLoading = true
        val gitHubService: GitHubService = GitHubRetrofitAdapter.getRetrofitInstance().create(GitHubService::class.java)
        val call: Call<Any> = gitHubService.repositoriesLanguages(languageUrl!!)
        call.enqueue(object : Callback<Any> {
            override fun onFailure(call: Call<Any>?, t: Throwable?) {
                languagesModel.isLoading = false
            }

            override fun onResponse(call: Call<Any>?, response: Response<Any>?) {
                if(response?.body() != null){
                    val languageMap : LinkedTreeMap<String, Double> = response?.body() as LinkedTreeMap<String, Double>
                    val languagesList : ArrayList<ItemRepositoryLangauageModel> = ArrayList()
                    for (entry in languageMap) {
                        val language = entry.key
                        val languageId : Double = entry.value
                        val langaugeModelItem: ItemRepositoryLangauageModel = ItemRepositoryLangauageModel()
                        langaugeModelItem.language = language
                        langaugeModelItem.languageID = languageId
                        languagesList.add(langaugeModelItem)
                    }
                    languagesModel.isLoading = false
                    languagesModel.updateLanguagesList(languagesList)
                }

            }
        })
    }
}