package com.github.github.details.contributors.models

import android.databinding.Bindable
import com.github.github.BR
import com.github.github.base.BaseViewModel
import com.github.github.details.contributors.responses.RepositoryContributorsResponseModel

/**
 * Created by hp on 27-05-2018.
 *
 * Observable for the items the contributors adapter
 */
class ItemRepositoryContributorModel : BaseViewModel() {
    var login: String? = null
        @Bindable get() = field
        set(login) {
            field = login
            notifyPropertyChanged(BR.login)
        }
    var id: Int? = null
        @Bindable get() = field
        set(id) {
            field = id
            notifyPropertyChanged(BR.id)
        }
    var avatarUrl: String? = null
        @Bindable get() = field
        set(avatarUrl) {
            field = avatarUrl
            notifyPropertyChanged(BR.avatarUrl)
        }
    var gravatarId: String? = null
        @Bindable get() = field
        set(gravatarId) {
            field = gravatarId
            notifyPropertyChanged(BR.gravatarId)
        }
    var url: String? = null

        @Bindable get() = field
        set(url) {
            field = url
            notifyPropertyChanged(BR.url)
        }
    var htmlUrl: String? = null
        @Bindable get() = field
        set(htmlUrl) {
            field = htmlUrl
            notifyPropertyChanged(BR.htmlUrl)
        }
    var followersUrl: String? = null
        @Bindable get() = field
        set(followersUrl) {
            field = followersUrl
            notifyPropertyChanged(BR.followersUrl)
        }
    var followingUrl: String? = null
        @Bindable get() = field
        set(followingUrl) {
            field = followingUrl
            notifyPropertyChanged(BR.followingUrl)
        }
    var gistsUrl: String? = null
        @Bindable get() = field
        set(gistsUrl) {
            field = gistsUrl
            notifyPropertyChanged(BR.gistsUrl)
        }
    var starredUrl: String? = null
        @Bindable get() = field
        set(starredUrl) {
            field = starredUrl
            notifyPropertyChanged(BR.starredUrl)
        }
    var subscriptionsUrl: String? = null

        @Bindable get() = field
        set(subscriptionsUrl) {
            field = subscriptionsUrl
            notifyPropertyChanged(BR.subscriptionsUrl)
        }
    var organizationsUrl: String? = null
        @Bindable get() = field
        set(organizationsUrl) {
            field = organizationsUrl
            notifyPropertyChanged(BR.organizationsUrl)
        }
    var reposUrl: String? = null
        @Bindable get() = field
        set(reposUrl) {
            field = reposUrl
            notifyPropertyChanged(BR.reposUrl)
        }
    var eventsUrl: String? = null
        @Bindable get() = field
        set(eventsUrl) {
            field = eventsUrl
            notifyPropertyChanged(BR.eventsUrl)
        }
    var receivedEventsUrl: String? = null
        @Bindable get() = field
        set(receivedEventsUrl) {
            field = receivedEventsUrl
            notifyPropertyChanged(BR.receivedEventsUrl)
        }
    var type: String? = null
        @Bindable get() = field
        set(type) {
            field = type
            notifyPropertyChanged(BR.type)
        }
    var siteAdmin: Boolean? = null
        @Bindable get() = field
        set(siteAdmin) {
            field = siteAdmin
            notifyPropertyChanged(BR.siteAdmin)
        }

    var contributions: Int? = null
        @Bindable get() = field
        set(contributions) {
            field = contributions
            notifyPropertyChanged(BR.contributions)
        }

    fun setContributorModel(repositoryContributorsResponseModel: RepositoryContributorsResponseModel?){
        login = repositoryContributorsResponseModel?.login
        siteAdmin = repositoryContributorsResponseModel?.siteAdmin
        id = repositoryContributorsResponseModel?.id
        receivedEventsUrl = repositoryContributorsResponseModel?.receivedEventsUrl
        reposUrl = repositoryContributorsResponseModel?.reposUrl
        avatarUrl = repositoryContributorsResponseModel?.avatarUrl
        gravatarId = repositoryContributorsResponseModel?.gravatarId
        followersUrl = repositoryContributorsResponseModel?.followersUrl
        followingUrl = repositoryContributorsResponseModel?.followingUrl
        contributions = repositoryContributorsResponseModel?.contributions
    }
}