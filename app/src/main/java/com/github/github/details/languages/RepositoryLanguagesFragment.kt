package com.github.github.details.languages

import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.github.github.R
import com.github.github.base.BaseFragment
import com.github.github.databinding.FragmentLanguagesLayoutBinding
import com.github.github.utils.AppConstants

/**
 * Created by hp on 26-05-2018.
 */
class RepositoryLanguagesFragment() : BaseFragment<FragmentLanguagesLayoutBinding>() {

    private var languageUrl: String? = null
    lateinit var languagePresenter: RepositoryLanguagesPresenter
    private lateinit var languageAdapter: LanguageRecyclerAdapter

    companion object {
        fun getInstance(bundle: Bundle): RepositoryLanguagesFragment {
            val repositoryLanguagesFragment: RepositoryLanguagesFragment = RepositoryLanguagesFragment()
            repositoryLanguagesFragment.arguments = bundle
            return repositoryLanguagesFragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val bundle: Bundle? = arguments
        languageUrl = bundle?.getString(AppConstants.URL)
    }

    override fun onBindViewInflated() {
        super.onBindViewInflated()
        languagePresenter = RepositoryLanguagesPresenter(RepositoryLanguageModel())
        viewDataBinding.presenter = languagePresenter
    }

    override fun getFragmentLayout(): Int {
        return R.layout.fragment_languages_layout
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        callLanguageApi()
    }

    private fun init() {
        languageAdapter = LanguageRecyclerAdapter(ArrayList<ItemRepositoryLangauageModel>())
        val rvLanguages: RecyclerView = viewDataBinding.recyclerViewLanguages
        rvLanguages.layoutManager = LinearLayoutManager(context)
        rvLanguages.addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.VERTICAL))
        rvLanguages.adapter = languageAdapter
    }

    private fun callLanguageApi() {
        languagePresenter.callRepositoryLanguageApi(languageUrl)
    }
}