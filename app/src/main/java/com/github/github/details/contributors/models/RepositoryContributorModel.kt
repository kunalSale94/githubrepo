package com.github.github.details.contributors.models

import android.databinding.ObservableArrayList
import com.github.github.base.BaseViewModel

/**
 * Created by hp on 27-05-2018.
 *
 * Holds the List of Observable for Contributors
 */
class RepositoryContributorModel : BaseViewModel() {

    var contributorListViewModelsListItem: ObservableArrayList<ItemRepositoryContributorModel> = ObservableArrayList()
        get() = field

    fun updateContributorsList(repositoriesViewModelItems:ArrayList<ItemRepositoryContributorModel>){
        contributorListViewModelsListItem.clear()
        contributorListViewModelsListItem.addAll(repositoriesViewModelItems)
    }
}