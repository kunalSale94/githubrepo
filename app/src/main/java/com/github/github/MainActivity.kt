package com.github.github

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.support.v7.app.AppCompatActivity
import com.github.github.details.RepositoriesDetailsFragment
import com.github.github.repositories.RepositoriesFragment
import com.github.github.utils.AppConstants
import com.github.github.utils.IFragmentTransactionListener

/**
 * Created by hp on 24-05-2018.
 */
class MainActivity : AppCompatActivity(), IFragmentTransactionListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if(savedInstanceState == null)
        {
            supportFragmentManager.beginTransaction().add( R.id.container, RepositoriesFragment()).commit()
        }
    }

    override fun onFragmentTransaction(bundle: Bundle) {
        val transaction : String? = bundle.getString(AppConstants.TRANSACTION)
        val backStack : Boolean? = bundle.getBoolean(AppConstants.BACKSTACK)
        val screenName : String? = bundle.getString(AppConstants.SCREENNAME)
        var fragment : Fragment? = null

        when(screenName){
            AppConstants.ScreenName.RepositoryDetailFragment->fragment = RepositoriesDetailsFragment()
        }

        fragment?.arguments = bundle
        if(transaction?.equals(AppConstants.REPLACE_FRAGMENT)!!){
            commitTransaction(backStack,replaceFragment(fragment))
        }
        else if(transaction.equals(AppConstants.ADD_FRAGMENT)){
            commitTransaction(backStack,addFragment(fragment))
        }
    }

    private fun replaceFragment(fragment : Fragment?) : FragmentTransaction?{
        return supportFragmentManager?.beginTransaction()?.replace(R.id.container,fragment)
    }

    private fun addFragment(fragment : Fragment?) : FragmentTransaction?{
        return supportFragmentManager?.beginTransaction()?.add(R.id.container,fragment)
    }

    private fun commitTransaction(backStack: Boolean?,fragmentTransaction : FragmentTransaction?){
        if(backStack!!)
        {
            fragmentTransaction?.addToBackStack(null)
        }
        fragmentTransaction?.commit()
    }
}