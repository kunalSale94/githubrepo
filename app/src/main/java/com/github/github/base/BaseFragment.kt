package com.github.github.base

import android.content.Context
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.github.github.utils.IFragmentTransactionListener

/**
 * Created by hp on 26-05-2018.
 */
abstract class BaseFragment<T : ViewDataBinding> : Fragment() {

    protected lateinit var viewDataBinding : T
    protected lateinit var fragmentTransactionListener : IFragmentTransactionListener

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        try {
            fragmentTransactionListener = context as IFragmentTransactionListener
        } catch (e : ClassCastException) {
            throw ClassCastException(context.toString() + " must implement IFragmentTransactionListener")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        viewDataBinding = DataBindingUtil.inflate( inflater!!, getFragmentLayout(), container, false)
        onBindViewInflated()
        return viewDataBinding.root
    }

    // method to action before view getting return from binding
    open fun onBindViewInflated(){
    }
    // getting layout
    abstract fun getFragmentLayout() : Int
}