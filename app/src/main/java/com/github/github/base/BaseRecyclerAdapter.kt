package com.github.github.base

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.github.github.databinding.ItemRepositoryRowLayoutBinding
import com.github.github.utils.IRecyclerClickedListener

/**
 * Created by hp on 25-05-2018.
 *
 *  BaseRecyclerAdapter is used to implement the common functionlity of recycleradapter
 *
 *  T : ViewModel Type
 *  VDB : ViewDataBinding for item row
 *  VH : ViewHolder of child
 */
abstract class BaseRecyclerAdapter<T : BaseViewModel, VDB : ViewDataBinding,VH : RecyclerView.ViewHolder>(protected val itemsList:ArrayList<T>) : RecyclerView.Adapter<VH>() {

    protected lateinit var viewDataBinding : VDB
    var itemClickedListener:IRecyclerClickedListener? = null

    // Create the ViewHolder for the subclass and gets the layout from subclass
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        viewDataBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), getItemLayout(), parent, false)
        return getViewHolder(viewDataBinding)
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.itemView.setOnClickListener({ itemClickedListener?.onRecyclerItemClicked(holder.adapterPosition) })
    }

    // Updates the list when there is change in the observable list
    open fun updateList(itemList:ArrayList<T>){
        itemsList.clear()
        itemsList.addAll(itemList)
        notifyDataSetChanged()
    }

    abstract fun getItemLayout():Int
    abstract fun getViewHolder(viewDataBinding: VDB):VH
}