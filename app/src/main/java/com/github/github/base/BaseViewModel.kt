package com.github.github.base

import android.databinding.BaseObservable
import android.databinding.Bindable
import com.github.github.BR

/**
 * Created by hp on 25-05-2018.
 */

open class BaseViewModel : BaseObservable() {
    var isLoading: Boolean = false
        @Bindable get() = field
        set(value) {
            field = value
            notifyPropertyChanged(BR.loading)
        }
}
