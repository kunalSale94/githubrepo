package com.github.github.base

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.app.FragmentStatePagerAdapter
import com.github.github.utils.PagerViewModel

/**
 * Created by hp on 26-05-2018.
 */
class DetailsPagerAdapter<T:Fragment>(pagerList:ArrayList<PagerViewModel<T>>,fragmentManager: FragmentManager) : FragmentStatePagerAdapter(fragmentManager) {

    var pagerList : ArrayList<PagerViewModel<T>>

    init {
        this.pagerList = pagerList
    }

    override fun getItem(position: Int): Fragment {
        return pagerList.get(position).fragment
    }

    override fun getCount(): Int {
        return pagerList.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        super.getPageTitle(position)
        return pagerList.get(position).title
    }
}