package com.github.github.repositories

import android.support.v7.widget.RecyclerView
import com.github.github.R
import com.github.github.base.BaseRecyclerAdapter
import com.github.github.databinding.ItemRepositoryRowLayoutBinding
import com.github.github.repositories.models.ItemRepositoriesModel

/**
 * Created by hp on 24-05-2018.
 */
class RepositoriesRecyclerAdapter(items: ArrayList<ItemRepositoriesModel>) : BaseRecyclerAdapter<ItemRepositoriesModel, ItemRepositoryRowLayoutBinding, RepositoriesRecyclerAdapter.RepositoriesViewHolder>(itemsList = items) {

    override fun getViewHolder(viewDataBinding: ItemRepositoryRowLayoutBinding): RepositoriesViewHolder {
        return RepositoriesViewHolder(viewDataBinding)
    }

    override fun getItemLayout(): Int {
        return R.layout.item_repository_row_layout;
    }

    override fun onBindViewHolder(holder: RepositoriesViewHolder, position: Int) {
        super.onBindViewHolder(holder, position)
        holder.viewDataBinding.viewModel = itemsList.get(holder.adapterPosition)
    }

    class RepositoriesViewHolder(viewDataBinding: ItemRepositoryRowLayoutBinding) : RecyclerView.ViewHolder(viewDataBinding.root) {
        var viewDataBinding: ItemRepositoryRowLayoutBinding

        init {
            this.viewDataBinding = viewDataBinding
        }
    }

    override fun updateList(itemList: ArrayList<ItemRepositoriesModel>) {
        itemsList.addAll(itemList)
        notifyDataSetChanged()
    }
}