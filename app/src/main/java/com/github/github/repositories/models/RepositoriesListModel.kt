package com.github.github.repositories.models

import android.databinding.Bindable
import android.databinding.ObservableArrayList
import com.github.github.base.BaseViewModel

/**
 * Created by hp on 26-05-2018.
 */
class RepositoriesListModel : BaseViewModel() {

    var itemRepositoriesListModelsList: ObservableArrayList<ItemRepositoriesModel> = ObservableArrayList()
        @Bindable get() = field

    fun updateRepositoriesList(itemRepositoriesModels: ArrayList<ItemRepositoriesModel>) {
        itemRepositoriesListModelsList.addAll(itemRepositoriesModels)
    }
}