package com.github.github.repositories

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.github.github.R
import com.github.github.base.BaseFragment
import com.github.github.databinding.FragmentRepositoriesLayoutBinding
import com.github.github.details.RepositoryDetailModel
import com.github.github.repositories.models.ItemRepositoriesModel
import com.github.github.utils.AppConstants
import com.github.github.utils.IRecyclerClickedListener

/**
 * Created by hp on 24-05-2018.
 */
class RepositoriesFragment : BaseFragment<FragmentRepositoriesLayoutBinding>(), IRecyclerClickedListener {

    private lateinit var repositoriesAdapter: RepositoriesRecyclerAdapter
    private lateinit var repositoriesPresenter: RepositoriesPresenter
    private lateinit var repositoriesViewModel : com.github.github.repositories.viewmodels.RepositoriesViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Get the ViewModel
        repositoriesViewModel = ViewModelProviders.of(this).get(com.github.github.repositories.viewmodels.RepositoriesViewModel::class.java)
        repositoriesPresenter = RepositoriesPresenter(repositoriesViewModel)
    }

    override fun getFragmentLayout(): Int {
        return R.layout.fragment_repositories_layout
    }

    override fun onBindViewInflated() {
        super.onBindViewInflated()
        viewDataBinding.presenter = repositoriesPresenter

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    override fun onResume() {
        super.onResume()
        repositoriesAdapter.itemClickedListener = this
    }

    private fun init() {
        setRecyclerApapter()
        addPaginationListener()
        if(repositoriesViewModel.getItems() == null || repositoriesViewModel.getItems()?.size == 0){
            repositoriesPresenter.getGitHubPublicRepository(0)
        }
        else{
            repositoriesPresenter.repositoriesListModel.updateRepositoriesList(repositoriesViewModel.getItems()!!)
        }
    }

    private fun setRecyclerApapter() {
        repositoriesAdapter = RepositoriesRecyclerAdapter(ArrayList())
        val rvRepositories: RecyclerView? = viewDataBinding.recyclerViewRepositories
        rvRepositories?.layoutManager = LinearLayoutManager(context)
        rvRepositories?.addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.VERTICAL))
        rvRepositories?.adapter = repositoriesAdapter
    }

    // To do pagination for the repositories
    private fun addPaginationListener() {
        viewDataBinding.recyclerViewRepositories.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                val linearLayoutManager = recyclerView?.layoutManager as LinearLayoutManager
                val visibleItemCount = linearLayoutManager.getChildCount()
                val totalItemCount = linearLayoutManager.getItemCount()
                val firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition()

                if (visibleItemCount + firstVisibleItemPosition == totalItemCount - 2 && firstVisibleItemPosition >= 0) {
                    repositoriesPresenter.getGitHubPublicRepository( totalItemCount - 2)
                }
            }
        })
    }

    override fun onStop() {
        super.onStop()
        repositoriesAdapter.itemClickedListener = null
    }

    override fun onRecyclerItemClicked(position: Int) {
        val repositoryModelItem: ItemRepositoriesModel = repositoriesPresenter.getRepositoryViewModel(position)
        val bundle: Bundle = Bundle()
        bundle.putString(AppConstants.TRANSACTION, AppConstants.REPLACE_FRAGMENT)
        bundle.putBoolean(AppConstants.BACKSTACK, true)
        bundle.putString(AppConstants.SCREENNAME, AppConstants.ScreenName.RepositoryDetailFragment)
        bundle.putParcelable(AppConstants.Models.RepositoryDetailModel, getRepositoryDetailsModel(repositoryModelItem))
        fragmentTransactionListener.onFragmentTransaction(bundle)
    }

    private fun getRepositoryDetailsModel(repositoryModelItem: ItemRepositoriesModel) : RepositoryDetailModel{
        val repositoryDetailModel : RepositoryDetailModel = RepositoryDetailModel()
        repositoryDetailModel.branchesUrl = repositoryModelItem.branchesUrl
        repositoryDetailModel.contributorsUrl = repositoryModelItem.contributorsUrl
        repositoryDetailModel.languagesUrl = repositoryModelItem.languagesUrl
        return repositoryDetailModel
    }
}
