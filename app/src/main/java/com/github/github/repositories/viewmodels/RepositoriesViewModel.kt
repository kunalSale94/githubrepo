package com.github.github.repositories.viewmodels

import android.arch.lifecycle.ViewModel
import com.github.github.repositories.models.ItemRepositoriesModel

/**
 * Created by hp on 28-05-2018.
 *
 *  ViewModel for Repositories Public List which persist the data across configuration changes
 */
class RepositoriesViewModel : ViewModel() {
    var itemRepositoriesModelsList: ArrayList<ItemRepositoriesModel> ?= null

    fun setItemRepositories(itemRepositoriesModelsList: ArrayList<ItemRepositoriesModel>) {
        if(this.itemRepositoriesModelsList == null){
            this.itemRepositoriesModelsList = ArrayList()
        }
        this.itemRepositoriesModelsList?.clear();
        this.itemRepositoriesModelsList?.addAll(itemRepositoriesModelsList)
    }

    fun getItems() = itemRepositoriesModelsList
}