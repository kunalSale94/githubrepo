package com.github.github.repositories.models

import android.databinding.Bindable
import android.os.Parcel
import android.os.Parcelable
import com.github.github.BR
import com.github.github.base.BaseViewModel
import com.github.github.repositories.responses.RepositoriesReponseModel

/**
 * Created by hp on 26-05-2018.
 */
class ItemRepositoriesModel() : BaseViewModel() {
    var id: Int? = null
        @Bindable get() = field
        set(id) {
            field = id
            notifyPropertyChanged(BR.id)
        }
    var owner: OwnerViewModel? = null
        @Bindable get() = field
        set(id) {
            field = id
            notifyPropertyChanged(BR.id)
        }
    var name: String? = null
        @Bindable get() = field
        set(name) {
            field = name
            notifyPropertyChanged(BR.name)
        }
    var fullName: String? = null
        @Bindable get() = field
        set(fullName) {
            field = fullName
            notifyPropertyChanged(BR.fullName)
        }
    var htmlUrl: String? = null
        @Bindable get() = field
        set(htmlUrl) {
            field = htmlUrl
            notifyPropertyChanged(BR.htmlUrl)
        }
    var description: String? = null
        @Bindable get() = field
        set(description) {
            field = description
            notifyPropertyChanged(BR.description)
        }
    var branchesUrl: String? = null
        @Bindable get() = field
        set(branchesUrl) {
            field = branchesUrl
            notifyPropertyChanged(BR.branchesUrl)
        }
    var contributorsUrl: String? = null
        @Bindable get() = field
        set(contributorsUrl) {
            field = contributorsUrl
            notifyPropertyChanged(BR.contributorsUrl)
        }
    var languagesUrl: String? = null
        @Bindable get() = field
        set(languagesUrl) {
            field = languagesUrl
            notifyPropertyChanged(BR.languagesUrl)
        }

    constructor(parcel: Parcel) : this() {
    }

    fun setReponseViewModel(repositoriesReponseModel: RepositoriesReponseModel){
        id = repositoriesReponseModel.id
        name = repositoriesReponseModel.name
        fullName = repositoriesReponseModel.fullName
        languagesUrl = repositoriesReponseModel.languagesUrl
        contributorsUrl = repositoriesReponseModel.contributorsUrl
        branchesUrl = repositoriesReponseModel.branchesUrl?.replace("{/branch}","")
        description = repositoriesReponseModel.description
        owner = OwnerViewModel()
        owner?.setOwnerViewModel(repositoriesReponseModel.owner)
    }
}