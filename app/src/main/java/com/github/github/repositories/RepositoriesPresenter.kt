package com.github.github.repositories

import com.github.github.data.GitHubRetrofitAdapter
import com.github.github.data.GitHubService
import com.github.github.repositories.models.RepositoriesListModel
import com.github.github.repositories.models.ItemRepositoriesModel
import com.github.github.repositories.responses.RepositoriesReponseModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by hp on 25-05-2018.
 */
class RepositoriesPresenter(repositoriesViewModel: com.github.github.repositories.viewmodels.RepositoriesViewModel ) {

    val repositoriesViewModel : com.github.github.repositories.viewmodels.RepositoriesViewModel
    val repositoriesListModel: RepositoriesListModel = RepositoriesListModel()

    init {
        this.repositoriesViewModel = repositoriesViewModel
    }

    fun getGitHubPublicRepository(since: Int) {
        repositoriesListModel.isLoading = true
        val gitHubService: GitHubService = GitHubRetrofitAdapter.getRetrofitInstance().create(GitHubService::class.java)
        val call: Call<List<RepositoriesReponseModel>> = gitHubService.publicRepositories(since)
        call.enqueue(object : Callback<List<RepositoriesReponseModel>> {
            override fun onFailure(call: Call<List<RepositoriesReponseModel>>?, t: Throwable?) {
                repositoriesListModel.isLoading = false
            }

            override fun onResponse(call: Call<List<RepositoriesReponseModel>>?, response: Response<List<RepositoriesReponseModel>>?) {
                val repositoriesResponseModels: List<RepositoriesReponseModel>? = response?.body()
                val itemRepositoriesModels: ArrayList<ItemRepositoriesModel> = ArrayList()
                if (repositoriesResponseModels != null) {
                    for (respositoriesResponseModel in repositoriesResponseModels) {
                        val itemRepositoriesModel: ItemRepositoriesModel = ItemRepositoriesModel()
                        itemRepositoriesModel.setReponseViewModel(respositoriesResponseModel)
                        itemRepositoriesModels.add(itemRepositoriesModel)
                    }
                }
                repositoriesListModel.isLoading = false
                repositoriesListModel.updateRepositoriesList(itemRepositoriesModels)
                repositoriesViewModel.setItemRepositories(repositoriesListModel.itemRepositoriesListModelsList)
            }
        })
    }

    fun getRepositoryViewModel(position: Int): ItemRepositoriesModel {
        val size : Int = repositoriesListModel.itemRepositoriesListModelsList.size
        if(position < size){
            return repositoriesListModel.itemRepositoriesListModelsList.get(position)
        }
        return ItemRepositoriesModel()
    }
}