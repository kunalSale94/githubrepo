package com.github.github.data

import com.github.github.details.branches.responses.RepositoryBranchesResponseModel
import com.github.github.details.contributors.responses.RepositoryContributorsResponseModel
import com.github.github.repositories.responses.RepositoriesReponseModel
import com.github.github.utils.AppUrls
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url

/**
 * Created by hp on 24-05-2018.
 */
interface GitHubService {

    // Public Repositories Api Call
    @GET("/repositories")
    fun publicRepositories(@Query("since")since:Int): Call<List<RepositoriesReponseModel>>

    // Repository Contributor Api Call
    @GET
    fun repositoriesContributors(@Url() url:String): Call<List<RepositoryContributorsResponseModel>>

    // Public Branches Api Call
    @GET
    fun repositoriesBranches(@Url() url:String): Call<List<RepositoryBranchesResponseModel>>

    // Public Languages Api Call
    @GET
    fun repositoriesLanguages(@Url() url:String): Call<Any>
}