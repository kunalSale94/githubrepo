package com.github.github.data

import com.github.github.utils.AppUrls
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by hp on 24-05-2018.
 */
class GitHubRetrofitAdapter private constructor(){
    companion object {
        lateinit var retrofit:Retrofit
        fun getRetrofitInstance():Retrofit{
            retrofit = Retrofit.Builder().baseUrl(AppUrls.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
            return retrofit
        }
    }
}